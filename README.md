# AlphaNgTest

Generate an Angular (2+) web application that on load displays namedays for a specific date in different countries.

The page will have a layout similar to this:

![layout](./layout.png "layout")

+ A: this is a datepicker
+ B: these are items in a list of results
+ C: name of the country
+ D: namedays for the country and the date
+ E: icon/check mark when there is more than one nameday for that day and country (i.e., for 4th of June in Spain, namedays are Emma, Quirino, Ruth, Saturnina, Noemí and Ruth, so there will be an icon here, but for Italy, it’s only Quirino, so no icon).
You can use the following API to get the data

https://api.abalin.net/namedays?day={day}&month={month}

This is strictly a frontend task, no backend has to be developed for this.

## How to

As this will be a timed exercise, you might not have time to finish everything as you would like, so you can follow this guide to achieve small goals incrementally.

+ Application must compile and run, no errors or warnings. Does not need to be build for production, but no weird messages in console.
+ Test your connection with the API. On load, display the data returned by the API for a specific date, no format or styling, just the json.
+ Add the datepicker and make it functional. You can click on it and select a specific date. You could print that date in the screen. No need to modify the data from the API yet.
+ Use the data selected in the datepicker to query the API and print the results, no format.
If you got here, congratulations, your app is functional, you can select a date in the datepicker and the results are displayed in the screen. Let's make it better.

+ Add some format and type checking. Create a viewmodel for the data presented in the screen only with the info you need and use that instead of the raw json.
+ The only missing thing is the icon. Go for it, put an icon or a mark for those elements with more than 1 nameday. Try to make it as efficient as possible.
+ Last, check what happens if we try to access another url in the app; instead of http://localhost:4200 (asuming you use that port for testing) what happens if you type http://localhost:4200/data? can we show an error screen instead?
From now on it's just unit testing and styling. Do your best!

## Considerations

+ Feel free to use any tool or base project to help you create your app, but please remove everything that you won’t use.
+ You can use all the libraries that you need to help you do the task (datepicker, style,…).
+ Styling won’t be evaluated, but performance will.
+ There is only one page, but the app should always redirect to the home page if the user tries to access any other page.
+ It’s up to you to decide how to structure the application and what elements are needed.

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 1.7.2.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).