import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { NamedaysResponseModel } from '../models/namedays-response.model';

@Injectable()
export class ApiService {

  constructor(
    private http: HttpClient
  ) {}

  public makeRequest(day: number, month: number): Promise<any> {
    return new Promise(
      (resolve, reject) => {
        const url = 'https://api.abalin.net/get/namedays';
        this.http.get(`${url}?day=${day}&month=${month}`).subscribe(
          (response: any) => {
            resolve(new NamedaysResponseModel(response.data));
          },
          (error) => {
            reject(error);
          }
        );
      }
    );
  }

  public namesOnly(data: NamedaysResponseModel): string[][] {
    return Object.entries(data).filter((elem) => elem[0].indexOf('name_') === 0);
  }
}
