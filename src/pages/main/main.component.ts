import { Component } from '@angular/core';
import { NamedaysResponseModel } from '../../models/namedays-response.model';
import { ApiService } from '../../services/api.service';

@Component({
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.scss']
})
export class MainComponent {

  public listElems: string[][];

  constructor(private apiService: ApiService) {
    this.listElems = [];
  }

  public dateChange(action: string, event: any): void {
    const date = new Date(event.value);
    const day = date.getDate();
    const month = date.getMonth() + 1;

    this.apiService.makeRequest(day, month).then(
      (res: NamedaysResponseModel) => {
        this.listElems = this.apiService.namesOnly(res);
        console.log(this.listElems);
      },
      (err) => {
        console.log(err);
      }
    );

  }
}
