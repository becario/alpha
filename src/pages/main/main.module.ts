import { NgModule } from '@angular/core';
import {
  MatDatepickerModule,
  MatNativeDateModule
} from '@angular/material';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientModule } from '@angular/common/http';
import { BrowserModule } from '@angular/platform-browser';

import { MainComponent } from './main.component';
import { ApiService } from '../../services/api.service';


@NgModule({
  declarations: [MainComponent],
  providers: [ApiService],
  imports: [
    BrowserModule,
    HttpClientModule,
    BrowserAnimationsModule,
    MatDatepickerModule,
    MatNativeDateModule
  ]
})
export class MainModule {}
