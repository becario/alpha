export class NamedaysResponseModel {
  public day: number;
  public month: number;
  public name_at: string;
  public name_cz: string;
  public name_de: string;
  public name_es: string;
  public name_fr: string;
  public name_hr: string;
  public name_hu: string;
  public name_it: string;
  public name_pl: string;
  public name_se: string;
  public name_sk: string;
  public name_us: string;

  constructor(args?: any) {
    Object.assign(this, ...args);
  }
}
