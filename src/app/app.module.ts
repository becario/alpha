import { NgModule } from '@angular/core';
import {
  RouterModule,
  Routes
} from '@angular/router';

import { AppComponent } from './app.component';
import { MainModule } from '../pages/main/main.module';
import { MainComponent } from '../pages/main/main.component';
import { NotFoundComponent } from '../pages/not-found/not-found.component';

const appRoutes: Routes = [
  {
    path: '',
    component: MainComponent
  },
  {
    path: '**',
    component: NotFoundComponent
  }
];

@NgModule({
  declarations: [
    AppComponent,
    NotFoundComponent
  ],
  imports: [
    RouterModule.forRoot(appRoutes),
    MainModule
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
